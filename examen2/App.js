import React, { useEffect, useState } from 'react';
import { Button, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native';
import ReactNativeAsyncStorage from '@react-native-async-storage/async-storage';
import { getAnalytics } from "firebase/analytics";
import { initializeApp, getReactNativePersistence } from "firebase/app";
import { createUserWithEmailAndPassword, getAuth, onAuthStateChanged, signInWithEmailAndPassword, signOut } from 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyAPmutJFhJrEQ3SuCPyQULlhAyQuzwLsS4",
  authDomain: "loginfirebase1-25918.firebaseapp.com",
  projectId: "loginfirebase1-25918",
  storageBucket: "loginfirebase1-25918.appspot.com",
  messagingSenderId: "10185746117",
  appId: "1:10185746117:web:adc3a5903f92e7b7e7f2ac",
  measurementId: "G-PX58B6J015"
};

// Inicializa firebase 
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);



const AuthScreen = ({ email, setEmail, password, setPassword, isLogin, setIsLogin, handleAuthentication}) => {

  return(
    <View style={styles.authContainer}>
      <Text style={styles.title}>{isLogin ? 'Sign In': 'Sign Up'}</Text>
      <TextInput 
      style={styles.input}
      value={email}
      onChangeText={setEmail}
      placeholder='email'
      autoCapitalize='none'
      
      />
      <TextInput 
      style={styles.input}
      value={password}
      onChangeText={setPassword}
      placeholder='password'
      secureTextEntry
      />

      <View style={styles.buttonContainer}>
        <Button 
          title={isLogin ? 'Sign In' : 'Sign Up'}
          color='#3498DB'
          onPress={handleAuthentication}
        />
      </View>

      <View style={styles.buttonContainer} >
        <Text style={styles.toggleText} onPress={() => setIsLogin(!isLogin)}>
            {isLogin ? 'Need an account? Sign Up' : 'Alredy have an account? Sign in'}
        </Text>
      </View>
    </View>
  );
}



const AuthenticatedScreen = ({user, handleAuthentication}) => {
  return(
    <View style={styles.authContainer}>
      <Text style={styles.title}>Welcome!</Text>
      <Text style={styles.emailText}>{user.email}</Text>
      <Button color='#e74c3c' title='Log out' onPress={handleAuthentication} ></Button>
    </View>
  );
};


export default function App() {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [user, setUser] = useState(null);
  const [isLogin, setIsLogin] = useState(true);


  const auth = getAuth(app);

  useEffect ( ()=> {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setUser(user);
    });email
    return () => unsubscribe;
  }, [auth]);

  const handleAuthentication = async() => {
    try {
      if(user){
        console.log("User logged out succesfully!");
        await signOut(auth);
      }else {
        if(isLogin) {
          await signInWithEmailAndPassword(auth, email, password);
          console.log("user signed in succesfully!");
        } else {
          await createUserWithEmailAndPassword(auth, email, password);
          console.log("User created succesfully!");

        }
      }
    } catch (error) {
        console.log("Authentifiaction error: ", error.message);
  
    }
  
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {user ? (
        <AuthenticatedScreen user ={user} handleAuthentication={handleAuthentication} />
      ) : (
        <AuthScreen
        email={email}
        setEmail={setEmail}
        password={password}
        setPassword={setPassword}
        isLogin={isLogin}
        setIsLogin={setIsLogin}
        handleAuthentication={handleAuthentication}
        ></AuthScreen>
      )}
      
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    backgroundColor: '#f0f0f0',
  },
  authContainer: {
    width: '80%',
    maxWidth: 400,
    backgroundColor: '#fff',
    padding: 16,
    borderRadius: 8,
    elevation: 3,
  },
  title: {
    fontSize: 24,
    marginBottom: 16,
    textAlign: 'center',
  },
  input: {
    height: 40,
    borderColor: '#ddd',
    borderWidth: 1,
    marginBottom: 16,
    padding: 8,
    borderRadius: 4,
  },
  buttonContainer: {
    marginBottom: 16,
  },
  toggleText: {
    color: '#3498db',
    textAlign: 'center',
  },
  bottomContainer: {
    marginTop: 20,
  },
  emailText: {
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 20,
  },
});